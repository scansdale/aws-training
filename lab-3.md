# Lab 3 - Create a database layer in your Amazon VPC infrastructure

* Create RDS database
* Create and configure ALB (Application Load Balancer)
  * Create target group
  * Create ALB
* Review RDS DB instance meta data
* Test application connectivity
* Replicate across regions

## Review RDS DB instance meta data

* [Endpoint](aurora.cluster-cavgrrhf81qb.us-west-2.rds.amazonaws.com)

## Test application connectivity

* [Endpoint](LabAppALB-734006289.us-west-2.elb.amazonaws.com)
