# Module 1 - Architecting fundamentals

## How is AWS global infrastructure organised?

---

### What are Cloud services and why do we use them?

Managed computation hosting solutions that is accessed via the internet. Or a rain cloud, depending on who you ask.

* [AWS definition of Cloud Computing](https://aws.amazon.com/what-is-cloud-computing/?nc1=f_cc)
* Optimize Costs
* Minimize security vulnerabilities
* Reduce management complexity
* Scale seamlessly
* Increase innovation
* Accelerate time to market

### Architects responsibilities

* Plan
* Research
* Build

### Managed services vs unmanaged service

#### Unmanaged

##### Unmanaged AWS

* OS Installation
* Server maintenance
* Rack and stack
* Power, HVAC (Heat, Ventilation and Air Conditioning), net

##### Unmanaged user

* App optimization
* Scaling
* High availability
* Database backups
* Database software patches
* Database software installs
* OS Patches

#### Managed

##### Managed AWS

* Scaling
* High availability
* Database backups
* Database software patches
* Database software installs
* OS Patches
* OS Installation
* Server maintenance
* Rack and stack
* Power, HVAC (Heat, Ventilation and Air Conditioning), net

##### Managed user

* App optimization

### AWS Regions

* [AWS Documentation](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)
* Connects to other regions via AWS global network connection

#### Factors in deciding which region

* Governance
* Latency
* Service availability
* Cost

#### Availability zones

* Fault isolation
* Connected by redundant high speed network
* Contains clusters of data centers

#### Local zones

* Smaller, localized services that do not necessarily contain all AWS offerings

#### Edge locations

* Provides ability to serve static content or cache to users
* There are more edge locations than availability zones
* Can store data from any region

---

## How can we build according to best practices?

---

### AWS Well-Architected Framework

* [AWS Documentation](https://aws.amazon.com/architecture/well-architected/?wa-lens-whitepapers.sort-by=item.additionalFields.sortDate&wa-lens-whitepapers.sort-order=desc)
* Security
* Cost optimization
* Reliability
* Performance efficiency
* Operational excellence
* Sustainability
