# Module 12 - Edge Services

* What are some strategies to reduce high latency and secure my data center?
* Which services and tools should I explore to improve the performance of internet applications?
* Mu application requires static IP addresses and fas regional failover; what are my options?
* How can I use AWS services to run workloads in my own data center?
* How can I protect public-facing applications?

## Edge fundamentals

* [AWS Documentation](https://aws.amazon.com/getting-started/fundamentals-core-concepts/)

## CloudFront

* [AWS Documentation](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Introduction.html)

### Behaviors

* [AWS Documentation](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/RequestAndResponseBehavior.html)

## Global Accelerator

* [AWS Documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/transfer-acceleration.html)

## DDoS Protection

* [AWS Documentation](https://docs.aws.amazon.com/waf/latest/developerguide/ddos-overview.html)
* AWS Shield

## Outposts

* [AWS Documentation](https://aws.amazon.com/outposts/)
