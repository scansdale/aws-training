# Module 2 - Account security

* What are the best practices to manage access to AWS accounts and resources?
* Can I give users access only to the resource they need?
* How can I grant temporary access to resources?
* What is the best way to manage multiple accounts?

---

## How can we control access to resources?

---

### Root user

* Unrestricted access that cannot be restricted
* Enable MFA (Multi Factor Authentication) on the account
* Do not use the account for daily tasks
* Disable access key and security access key on the account

### IAM (Identity and Access Management) policies

* [AWS Documentation](https://aws.amazon.com/iam/)
* [AWS FAQ](https://aws.amazon.com/iam/faqs/)
* [AWS Quotas](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_iam-quotas.html)
* Create and manage users, groups and roles
* Manage access to AWS services and resources
* Analyze access controls

#### Principals

* Is an entity with permissions to request an action or operation on an AWS resource
* Users
* Groups
* Services
* Roles

### Users

* Specify how the user will interact with AWS resources
* Assign policies to users

### Groups

* Users can be members of multiple groups
* Assign policies to groups
* Up to **300** groups

### Roles

* [AWS Documentation](https://docs.aws.amazon.com/STS/latest/APIReference/welcome.html)
* Assign policies to roles
* Users and resources can assume roles
* Uses AWS STS (Security Token Services)
* Up to **1000** roles

### Policies

* [AWS Documentation](https://aws.amazon.com/iam/features/manage-permissions/)
* Effects
* Actions
* Resources
* Conditions

#### Types of permissions

* Set maximum permissions
* Grant permissions

##### Set maximum permissions

* [Permission boundaries](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_boundaries.html)
* Is overridden by Grant permissions
* Sets full permissions on resources

##### Grant permissions

* Permissions are deny by default
* IAM identify-based policies
  * Can be attached to user
  * Can be attached to group
  * Can be attached to role
* IAM resource-based policies
  * Can be attached to resource

### Organizational Units

* [AWS Documentation](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_ous.html)
* A collection of users
* Can have policies applied to them
