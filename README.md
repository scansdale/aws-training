# AWS Training

> CTO would like a solution for a POC which must have the following considerations.
>
> * How is AWS global infrastructure organised?
> * How can we build according to best practices?
> * How can we control access to resources?
> * Who or what will control where our resource run?

## Modules

* [Module 1 - Architecting fundamentals](./module-1.md)
* [Module 2 - Account security](./module-2.md)
* [Module 3 - Networking, Part 1](./module-3.md)
* [Module 4 - Compute](./module-4.md)
* [Module 5 - Storage](./module-5.md)
* [Module 6 - Database Services](./module-6.md)
* [Module 7 - Monitoring and Scaling](./module-7.md)
* [Module 8 - Automation](./module-8.md)
* [Module 9 - Containers](./module-9.md)
* [Module 10 - Networking Part 2](./module-10.md)
* [Module 11 - Serverless Architecture](./module-11.md)
* [Module 12 - Edge Services](./module-12.md)
* [Module 13 - Backup and recovery](./module-13.md)

## Labs

* [Lab 1 - Creating EC2 HTTP server instances](./lab-1.md)
* [Lab 2 - Build your Amazon VPC infrastructure](./lab-2.md)
* [Lab 3 - Create a database layer in your Amazon VPC infrastructure](./lab-3.md)
* [Lab 4 - Configure high availability in your Amazon VPC](./lab-4.md)
