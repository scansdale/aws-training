# Module 6 - Database Services

## Relational vs non-relational DB

## Amazon RDS (Relational Database Service)

* Managed service

### Engines supported

* Amazon Aurora
* PostgreSQL
* MAria DB
* MySQL
* Oracle
* SQL Server

### Multi-AZ deployments

* [AWS Documentation](https://aws.amazon.com/rds/features/multi-az/)
* Ensures uptime if an Availability Zone is unavailable
* Replicates data between instances in each AZ

### Read replicas

* [AWS Documentation](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_ReadRepl.html)
* Horizontally scale for read-heavy workloads
* Offload reporting
* Replicate across AWS Regions

### Data encryption at rest

* [AWS Documentation](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.Encryption.html)
* Managed by AWS KMS
* Unique data key encrypts your data
* AWS KMS key encrypts data keys
* Available for all RDS engines

### Aurora

* [AWS Documentation](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/CHAP_AuroraOverview.html)
* Built on MySQL and PostgreSQL

#### Clusters

* [AWS Documentation](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Aurora.Overview.html)

#### Serverless

* [AWS Documentation](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/aurora-serverless.html)
* Starts up on demand and shows down when not in use
* Scales up and down automatically

## DynamoDB

* [AWS Documentation](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html)
* Performance at scale
* No servers to manage
* Enterprise ready

## Redshift

* [AWS Documentation](https://docs.aws.amazon.com/redshift/index.html)
* Fully managed petabyte-scale data warehouse
* Online analytic processing (OLAP)
* Uses columnar data storage

### Data warehouse

* Data used for analytical purposes to gain business insights

## Caching

### Strategies

#### Lazy loading

1. Data request
1. Cache miss
1. Missing data request
1. Data turn
1. Returned value stored in cache

#### Write-through

1. Data write
1. Application sends data to cache

### ElastiCache

* [AWS Documentation](https://docs.aws.amazon.com/elasticache/)
* In-memory cache solution for fast response times

### Removing items from the cache

* Add a time to live (TTL) value to each application write
* The application queries the database for the data when the TTL expires

### DynamoDB Accelerator (DAX)

* [AWS Documentation](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DAX.html)

## AWS DMS (Database Migration Service)

* [AWS Documentation](https://docs.aws.amazon.com/dms/latest/userguide/Welcome.html)
* Heterogeneous database migrations
* Database consolidation
* Continuous data replication
* Can point to a database, Amazon S3, or other service
