# Lab 4 - Build a Serverless Architecture

* Create SNS topic
* Create three Amazon SQS queues and subscribe
* Create an Amazon S3 Event Notification
* Create Lambda functions
* Upload to S3 bucket

## Create SNS topic

* ARN: arn:aws:sns:us-west-2:135839438458:resize-image-topic-9232
* Topic owner: 135839438458
