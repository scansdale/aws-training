# Lab 2 - Build your Amazon VPC infrastructure

* Create VPC - 10.0.0.0/24
* Create Private Subnet - 10.0.0.0/16
* Create Public Subnet - 10.0.2.0/23
* Create Internet Gateway
* Attach Internet Gateway
* Route traffic via Internet Gateway to the Public Subnet
* Create a public security group
* Launch EC2 instance to use the Public Subnet
* Connect to website hosted on EC2
* Connect to EC2 using the Public Subnet
* Create a NAT gateway for Private Subnet
* Create a security group for Private Subnet
* Connect to EC2 using the Private Subnet

## Attach Internet Gateway

```bash
aws ec2 attach-internet-gateway --vpc-id "vpc-09777db865ce09f01" --internet-gateway-id "igw-0826059e3f482f21c" --region us-west-2
```

## Connect to website hosted on EC2

[EC2 Running](http://ec2-54-245-190-222.us-west-2.compute.amazonaws.com/)

## Connect to EC2 using the Public Subnet

```bash
cd ~
curl -I https://aws.amazon.com/training/
# HTTP/2 200
# content-type: text/html;charset=UTF-8
# server: Server
# date: Wed, 08 Dec 2021 09:14:07 GMT
# x-amz-rid: 4B9B9EFD5QG04B38MCP5
# set-cookie: aws-priv=eyJ2IjoxLCJldSI6MCwic3QiOjB9; Version=1; # Comment="Anonymous cookie for privacy regulations"; Domain=.aws.# amazon.com; Max-Age=31536000; Expires=Thu, 08-Dec-2022 09:14:07 # GMT; Path=/
# set-cookie: aws_lang=en; Domain=.amazon.com; Path=/
# x-frame-options: SAMEORIGIN
# x-xss-protection: 1; mode=block
# strict-transport-security: max-age=300
# x-content-type-options: nosniff
# x-amz-id-1: 4B9B9EFD5QG04B38MCP5
# last-modified: Tue, 07 Dec 2021 02:14:14 GMT
# content-security-policy-report-only: default-src *; connect-src # *; font-src * data:; frame-src *; img-src * data:; media-src *; # object-src *; script-src 'unsafe-eval' 'unsafe-inline' *; # style-src 'unsafe-inline' *; report-uri https://prod-us-west-2.# csp-report.marketing.aws.dev/submit
# vary: accept-encoding,Content-Type,Accept-Encoding,# X-Amzn-CDN-Cache,X-Amzn-AX-Treatment,User-Agent
# permissions-policy: interest-cohort=()
# x-cache: Miss from cloudfront
# via: 1.1 7f5e0d3b9ea85d0d75063a66c0ebc841.cloudfront.net # (CloudFront)
# x-amz-cf-pop: HIO50-C1
# x-amz-cf-id: # Ke34aTZlGJ-Di4dstAEVB-kmIW-D-TGafXNYCNOF0aaWtafqN8R-ag==
```

## Connect to EC2 using the Private Subnet

```bash
curl -I https://aws.amazon.com/training/
# HTTP/2 200
# content-type: text/html;charset=UTF-8
# server: Server
# date: Wed, 08 Dec 2021 09:33:04 GMT
# x-amz-rid: 32PEA65XENSNH9CVXEHE
# set-cookie: aws-priv=eyJ2IjoxLCJldSI6MCwic3QiOjB9; Version=1; # Comment="Anonymous cookie for privacy regulations"; Domain=.aws.# amazon.com; Max-Age=31536000; Expires=Thu, 08-Dec-2022 09:33:04 # GMT; Path=/
# set-cookie: aws_lang=en; Domain=.amazon.com; Path=/
# x-frame-options: SAMEORIGIN
# x-xss-protection: 1; mode=block
# strict-transport-security: max-age=300
# x-content-type-options: nosniff
# x-amz-id-1: 32PEA65XENSNH9CVXEHE
# last-modified: Tue, 07 Dec 2021 02:14:14 GMT
# content-security-policy-report-only: default-src *; connect-src # *; font-src * data:; frame-src *; img-src * data:; media-src *; # object-src *; script-src 'unsafe-eval' 'unsafe-inline' *; # style-src 'unsafe-inline' *; report-uri https://prod-us-west-2.# csp-report.marketing.aws.dev/submit
# vary: accept-encoding,Content-Type,Accept-Encoding,# X-Amzn-CDN-Cache,X-Amzn-AX-Treatment,User-Agent
# permissions-policy: interest-cohort=()
# x-cache: Miss from cloudfront
# via: 1.1 26ca01ec7377e425b59b6a08cb1ec343.cloudfront.net # (CloudFront)
# x-amz-cf-pop: HIO50-C1
# x-amz-cf-id: # 32LaDuoVqUF1zmhqX38hbYtnMMfWKeOWHrnj38dohZbewaNS_IV2NA==
```

## Optional challenge for ping

Solution is to add IPv4 ICMP to the security group allowed inbound rules
