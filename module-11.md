# Module 11 - Serverless Architecture

* How do I connect my applications to AWS services?
* How do I build an event-driven architecture?
* How do I make my application send text messages?
* How do I process large amounts of streaming data in near-real time?
* How do I coordinate API calls within a multiple step workflow?

## Serverless architectures

* Focus on development instead of maintenance
* Create loosely coupled and highly scalable workloads
* Reduce the total cost of ownership

## API Gateway

* [AWS Documentation](https://docs.aws.amazon.com/apigateway/latest/developerguide/welcome.html)
* Create an entry point for your applications
* Process thousands of concurrent API calls
* Choose internet facing or internal only

## Simple Queue Service (SQS)

* [AWS Documentation](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/welcome.html)
* Separates producer application tier from consumer application
* Maximum size of 256kb per request
* Use S3 of a large file and send the endpoint to the consumer application

### Types

* Standard
* First in first out (FIFO)

## Simple Notification Service (SNS)

* [AWS Documentation](https://docs.aws.amazon.com/sns/latest/dg/welcome.html)
* One to many

## Kinesis

* [AWS Documentation](https://aws.amazon.com/kinesis/getting-started/)

## How it works

1. Producers put data records into Kinesis Dat aStreams
1. Shards read and write data into streams for processing
1. Consumers process and store data in AWS storage services

## Step functions

* [AWS Documentation](https://docs.aws.amazon.com/step-functions/latest/dg/welcome.html)
