# Module 10 - Networking Part 2

* How do I keep my network secure?
* How do my services communicate with each other?
* What options do I have to set up a hybrid service?
* How do I manage traffic and security for rapidly expanding number of VPCs?
* What can I use for DNS resolution?

## VPC Endpoints

* There is no need for public internet access
* Endpoints are horizontally scaled, redundant and highly available

### Types

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/privatelink/vpc-endpoints.html)
* Gateway
  * Access via URL endpoint
    * Amazon S3
    * DynamoDB
* Interface
* Gateway Load Balancer

### Private link

* [Services that work with Private Link](https://docs.aws.amazon.com/vpc/latest/privatelink/integrated-services-vpce-list.html)
* Share to thousands of VPCs
* Use a security group for connection
* Can use overlapping addresses
* Private one-way access
* Share a single service

## VPC Peering

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/peering/what-is-vpc-peering.html)
* User private IP addresses
* Intra and inter-Region support
* IP spaces cannot overlap
* No transitive peering relationship
* Cross-account support

### VPC peering connections

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/peering/working-with-vpc-peering.html)
* Bypass the internet gateway or virtual gateway
* Use highly available connections - no single point of failure
* Avoid bandwidth bottlenecks
* Always stay on the global AWS backbone

## Transit Gateway

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/tgw/what-is-transit-gateway.html)
* Attachment based
* Flexible routing and segmentation
* Connectivity simplified
* Highly available and scalable
* Increased monitoring capabilities
* Multicast support
* Connects up to 5,000 VPCs and on-premises environments
* Acts as a hub for all traffic to flow through
* Permits multicast and inter-regional transit

### Transit Gateway setup

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/tgw/tgw-getting-started.html)
* Attach VPCs, VPN, Direct Connect gateway, and transit gateway peering connections

## Hybrid networking

* [AWS Documentation](https://docs.aws.amazon.com/whitepapers/latest/building-scalable-secure-multi-vpc-network-infrastructure/hybrid-connectivity.html)
* Managed connection
* Static or dynamic VPN

### Direct connect

* [AWS Documentation](https://docs.aws.amazon.com/directconnect/latest/UserGuide/Welcome.html)
* Create a fiber link from your data center into the backbone of Amazon
* Available from 1 Gbps to sub-1 Gbps links
* More predictable network performance
* Reduced bandwidth costs
* Associated with a Region

## Route 53

* [AWS Documentation](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/Welcome.html)
* DNS

### Routing policies

* [AWS Documentation](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-policy.html)
* Simple
* Failover
* Geolocation
* Geoproximity
* Latency-based
* Multivalue answer
* Weighted
