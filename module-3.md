# Module 3 - Networking, Part 1

* How do I build a dynamic and secure network infrastructure in my AWS accounts?
* How can I divide my private cloud into subnets and assign IP addresses?
* How can I direct and filter traffic moving in and out of a network?

## IP addressing

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html)
* Uses Classless Inter-Domain Routing ([CIDR](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing)) blocks
* IPV4 is a 32 bit address
* Each octet is 8 bits

### CIDR Blocks

* 0.0.0.0/0 - All IPs
* 10.22.33.44/8 - 10.\*.\*.\*
* 10.22.33.44/16 - 10.22.\*.\*
* 10.22.33.44/24 - 10.22.33.\*
* 10.22.33.44/32 - 10.22.33.44

### Private IP addresses

|     Name     |   CIDR block   |         Address range         | Number of addresses |           Classful description          |
|:------------:|:--------------:|:-----------------------------:|:-------------------:|:---------------------------------------:|
| 24-bit block | 10.0.0.0/8     | 10.0.0.0 – 10.255.255.255     | 16777216            | Single Class A.                         |
| 20-bit block | 172.16.0.0/12  | 172.16.0.0 – 172.31.255.255   | 1048576             | Contiguous range of 16 Class B blocks.  |
| 16-bit block | 192.168.0.0/16 | 192.168.0.0 – 192.168.255.255 | 65536               | Contiguous range of 256 Class C blocks. |

## VPC (Virtual Private Cloud) fundamentals

* [AWS Documentation](https://aws.amazon.com/vpc/)
* VPC's expand the availability zones within a region
* Subnet's are associated with a singular availability zone

### Private route tables

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Route_Tables.html)
* Best practice is to create your own routing table

### Subnets

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html)
* 5 IP addresses are reserved
* \*.\*.\*.0 - Network address
* \*.\*.\*.1 - Reserved by AWS for the VPC router
* \*.\*.\*.2 - Reserved by AWS
* \*.\*.\*.3 - Reserved by AWS for future use
* \*.\*.\*.255 - Network broadcast address
* Cannot expand availability zones

## NAT gateway

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html)
* Prevents communication with private subnet
* Communicate between public and private IP addresses

## VPC traffic security

### Network ACLs (Access Control List)

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html)
* Acts as a Firewall at the subnet boundary
* By default, it allows all inbound and outbound traffic
* It is stateless, requiring explicit rules for all traffic

#### Well known ports

* [Wikipedia article](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers)

### Security groups

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html)
* You can specify allow rules, but not deny rules
* You can specify separate rules for inbound and outbound traffic
* Security groups are stateful
* When you first create a security group, it has no inbound rules

## Reachability analyser

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/reachability/what-is-reachability-analyzer.html)
* Troubleshoot connectivity issues caused by network misconfiguration
* Verify that your network configuration matches your intended connectivity
* Automate the verification of your connectivity intent as your network configuration changes

## Internet Gateway

* [AWS Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html)
* Horizontally scaled, redundant, and highly available VPC component that allows communication between your VPC and the internet
