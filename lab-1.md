# Lab 1 - Explore using the AWS API to deploy an EC2 instance

* EC2 user data can be a script that executes to apply initial configuration

```text
#!/bin/bash -ex
sudo yum update -y
sudo yum install -y httpd php
sudo service httpd start
sudo systemctl enable httpd.service
cd /var/www/html
wget https://us-west-2-tcprod.s3.amazonaws.com/courses/ILT-TF-200-ARCHIT/v7.0.2/lab-1-EC2/scripts/instanceAdata.zip
unzip instanceAdata.zip
```

* EBS (Elastic Block Store) are disk volumes
* ENI (Elastic Network Interface)
* [Instance A website](http://ec2-34-222-27-195.us-west-2.compute.amazonaws.com/)

## Creating an instance from CLI

### Region

```bash
AZ=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
export AWS_DEFAULT_REGION=${AZ::-1}
echo $AWS_DEFAULT_REGION # us-west-2a
```

### AMI

```bash
AMI=$(aws ssm get-parameters --names /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 --query 'Parameters[0].[Value]' --output text)
echo $AMI # ami-0e21d4d9303512b8e
```

### Subnet

```bash
SUBNET=$(aws ec2 describe-subnets --filters 'Name=tag:Name,Values=LabPublicSubnet' --query Subnets[].SubnetId --output text)
echo $SUBNET # subnet-011429fd8a9af8cbc
```

### Security Group

```bash
SG=$(aws ec2 describe-security-groups --filters Name=tag:Name,Values=LabInstanceSecurityGroup --query SecurityGroups[].GroupId --output text)
echo $SG # sg-0abdca7aae6027f36
```

### User Data script

```bash
cd ~
wget https://us-west-2-tcprod.s3.amazonaws.com/courses/ILT-TF-200-ARCHIT/v7.0.2/lab-1-EC2/scripts/UserDataInstanceB.txt
cat UserDataInstanceB.txt
# #!/bin/bash -ex
# sudo yum update -y
# sudo yum install -y httpd php
# sudo service httpd start
# sudo systemctl enable httpd.service
# cd /var/www/html
# wget https://us-west-2-tcprod.s3.amazonaws.com/courses/# ILT-TF-200-ARCHIT/v7.0.2/lab-1-EC2/scripts/instanceBdata.zip

```

### Create new instance

```bash
INSTANCE=$(\
aws ec2 run-instances \
--image-id $AMI \
--subnet-id $SUBNET \
--security-group-ids $SG \
--user-data file://./UserDataInstanceB.txt \
--instance-type t2.micro \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=InstanceB}]' \
--query 'Instances[*].InstanceId' \
--output text \
)
echo $INSTANCE # i-02b762b095fb31f99
```

### Get instance information

```bash
aws ec2 describe-instances --instance-ids $INSTANCE
# {
#     "Reservations": [
#         {
#             "Groups": [],
#             "Instances": [
#                 {
#                     "AmiLaunchIndex": 0,
#                     "ImageId": "ami-0e21d4d9303512b8e",
#                     "InstanceId": "i-02b762b095fb31f99",
#                     "InstanceType": "t2.micro",
#                     "LaunchTime": "2021-12-07T13:12:12+00:00",
#                     "Monitoring": {
#                         "State": "disabled"
#                     },
#                     "Placement": {
#                         "AvailabilityZone": "us-west-2a",
#                         "GroupName": "",
#                         "Tenancy": "default"
#                     },
#                     "PrivateDnsName": "ip-10-1-11-236.us-west-2.compute.internal",
#                     "PrivateIpAddress": "10.1.11.236",
#                     "ProductCodes": [],
#                     "PublicDnsName": "ec2-34-219-189-184.us-west-2.compute.amazonaws.com",
#                     "PublicIpAddress": "34.219.189.184",
#                     "State": {
#                         "Code": 16,
#                         "Name": "running"
#                     },
#                     "StateTransitionReason": "",
#                     "SubnetId": "subnet-011429fd8a9af8cbc",
#                     "VpcId": "vpc-0fbc53a04d5c61221",
#                     "Architecture": "x86_64",
#                     "BlockDeviceMappings": [
#                         {
#                             "DeviceName": "/dev/xvda",
#                             "Ebs": {
#                                 "AttachTime": "2021-12-07T13:12:13+00:00",
#                                 "DeleteOnTermination": true,
#                                 "Status": "attached",
#                                 "VolumeId": "vol-04c6bd7918400f0ea"
#                             }
#                         }
#                     ],
#                     "ClientToken": "b4eaa799-bea6-4e6e-953a-a6b7fe1d0b3d",
#                     "EbsOptimized": false,
#                     "EnaSupport": true,
#                     "Hypervisor": "xen",
#                     "NetworkInterfaces": [
#                         {
#                             "Association": {
#                                 "IpOwnerId": "amazon",
#                                 "PublicDnsName": "ec2-34-219-189-184.us-west-2.compute.amazonaws.com",
#                                 "PublicIp": "34.219.189.184"
#                             },
#                             "Attachment": {
#                                 "AttachTime": "2021-12-07T13:12:12+00:00",
#                                 "AttachmentId": "eni-attach-078d79ecb80b74cbb",
#                                 "DeleteOnTermination": true,
#                                 "DeviceIndex": 0,
#                                 "Status": "attached",
#                                 "NetworkCardIndex": 0
#                             },
#                             "Description": "",
#                             "Groups": [
#                                 {
#                                     "GroupName": "qls-5131391-de71d21546e18df0-LabInstanceSecurityGroup-OCOS8I2IM079",
#                                     "GroupId": "sg-0abdca7aae6027f36"
#                                 }
#                             ],
#                             "Ipv6Addresses": [],
#                             "MacAddress": "06:20:b6:69:5c:7f",
#                             "NetworkInterfaceId": "eni-04fd6cf4eec32a9c1",
#                             "OwnerId": "220189416191",
#                             "PrivateDnsName": "ip-10-1-11-236.us-west-2.compute.internal",
#                             "PrivateIpAddress": "10.1.11.236",
#                             "PrivateIpAddresses": [
#                                 {
#                                     "Association": {
#                                         "IpOwnerId": "amazon",
#                                         "PublicDnsName": "ec2-34-219-189-184.us-west-2.compute.amazonaws.com",
#                                         "PublicIp": "34.219.189.184"
#                                     },
#                                     "Primary": true,
#                                     "PrivateDnsName": "ip-10-1-11-236.us-west-2.compute.internal",
#                                     "PrivateIpAddress": "10.1.11.236"
#                                 }
#                             ],
#                             "SourceDestCheck": true,
#                             "Status": "in-use",
#                             "SubnetId": "subnet-011429fd8a9af8cbc",
#                             "VpcId": "vpc-0fbc53a04d5c61221",
#                             "InterfaceType": "interface"
#                         }
#                     ],
#                     "RootDeviceName": "/dev/xvda",
#                     "RootDeviceType": "ebs",
#                     "SecurityGroups": [
#                         {
#                             "GroupName": "qls-5131391-de71d21546e18df0-LabInstanceSecurityGroup-OCOS8I2IM079",
#                             "GroupId": "sg-0abdca7aae6027f36"
#                         }
#                     ],
#                     "SourceDestCheck": true,
#                     "Tags": [
#                         {
#                             "Key": "Name",
#                             "Value": "InstanceB"
#                         }
#                     ],
#                     "VirtualizationType": "hvm",
#                     "CpuOptions": {
#                         "CoreCount": 1,
#                         "ThreadsPerCore": 1
#                     },
#                     "CapacityReservationSpecification": {
#                         "CapacityReservationPreference": "open"
#                     },
#                     "HibernationOptions": {
#                         "Configured": false
#                     },
#                     "MetadataOptions": {
#                         "State": "applied",
#                         "HttpTokens": "optional",
#                         "HttpPutResponseHopLimit": 1,
#                         "HttpEndpoint": "enabled",
#                         "HttpProtocolIpv6": "disabled"
#                     },
#                     "EnclaveOptions": {
#                         "Enabled": false
#                     },
#                     "PlatformDetails": "Linux/UNIX",
#                     "UsageOperation": "RunInstances",
#                     "UsageOperationUpdateTime": "2021-12-07T13:12:12+00:00",
#                     "PrivateDnsNameOptions": {
#                         "HostnameType": "ip-name",
#                         "EnableResourceNameDnsARecord": false,
#                         "EnableResourceNameDnsAAAARecord": false
#                     }
#                 }
#             ],
#             "OwnerId": "220189416191",
#             "ReservationId": "r-0370e544412e58bd5"
#         }
#     ]
# }
```

### Query instance state

```bash
aws ec2 describe-instances --instance-ids $INSTANCE --query 'Reservations[].Instances[].State.Name' --output text # running
```

### Get instance public DNS

```bash
aws ec2 describe-instances --instance-ids $INSTANCE --query Reservations[].Instances[].PublicDnsName --output text #ec2-34-219-189-184.us-west-2.compute.amazonaws.com
```

* [Instance B website](http://ec2-34-219-189-184.us-west-2.compute.amazonaws.com/)

## Creating an instance using CloudFormation

* [Example CloudFormation file](./cloudFormation/lab-1/Task3.yml)
* [Instance C website](http://ec2-34-219-235-31.us-west-2.compute.amazonaws.com/)

## Modifying an instance using CloudFormation

* [Example CloudFormation file](./cloudFormation/lab-1/optional.yml)
* [Instance D website](http://ec2-34-213-132-167.us-west-2.compute.amazonaws.com/)
