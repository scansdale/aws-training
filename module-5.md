# Module 5 - Storage

* How can I optimise storage costs and provide security while ensuring high availability?
* I want to reduce the cost of storage for images as they age. How can I automate the process?
* What are some options I can explore to build secure and scalable storage for both Windows and Linux?
* I have to move lots of data to the cloud in a relatively short time period - what are my options?

## Types of storage

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Storage.html)
* Block storage
* File storage
* Object storage

### S3

* [AWS Documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html)
* Inexpensive data storage
* Backup and restore
* Business-critical applications
* Archiving and compliance

### Versioning

* [AWS Documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Versioning.html)
* Protects files from accidental deletion
* Keep multiple variants of an objective in the same bucket
* Restore an object to a previous version

### Classes

* [AWS Documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/storage-class-intro.html)
* S3 Standard
  * Milliseconds to access
* S3 Intelligent-Tiering
  * Auto adjustment of costs based on usage
  * Milliseconds to access
* S3 Standard-IA
  * Milliseconds to access
* S3 One Zone-IA
  * Milliseconds to access
* S3 Glacier
  * Minutes or hours to access
* S3 Glacier Deep Archive
  * Hours to access

#### S3 Glacier retrieval times

* Expedited
* Standard
* Bulk

### Encryption at rest

* [AWS Documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/UsingEncryption.html)
* Can use AWS KMS
* Encrypts data

### Transfer Acceleration

* [AWS Documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/transfer-acceleration.html)
* Allows for secure movement of data to edge locations

### Multipart upload

* [AWS Documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/mpuoverview.html)

### Access points solutions

* [AWS Documentation](https://aws.amazon.com/s3/features/access-points/)
* Manage access at scale
* Each access point as a unique DNS name
* Each access point as distinct permissions and network controls

### Cost factors

* Anything going in is free
* Anything out to EC2 or CloudFront within the same region is free
* GBs per month for transferring out

### Best practices

* Storage classes and cost optimization
* Data protection
* Security
* Analytics and insights

### Event notification

* [AWS Documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/NotificationHowTo.html)
* Used to trigger an action when storage actions are performed

### EFS (Elastic File Storage)

* [AWS Documentation](https://docs.aws.amazon.com/efs/latest/ug/whatisefs.html)
* Across multiple availability zones
