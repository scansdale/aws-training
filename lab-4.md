# Lab 4 - Configure high availability in your Amazon VPC

* Inspecting your existing lab environment
* Creating a launch template
* Creating an Auto Scaling group
* Testing the application
* Testing high availability
* Configuring high availability of RDS

## Current user data

```bash
#!/bin/bash
yum -y update

# Install and enable AWS Systems Manager Agent
cd /tmp
yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
systemctl enable amazon-ssm-agent
systemctl start amazon-ssm-agent

# Install Apache Web Server and PHP
yum install -y httpd mysql
amazon-linux-extras install -y php7.2

# Download Inventory App Lab files
wget https://us-east-1-tcprod.s3.us-east-1.amazonaws.com/courses/ILT-TF-200-ARCHIT/v7.0.2/lab-4-HA/scripts/inventory-app.zip
unzip inventory-app.zip -d /var/www/html/

# Download and install the AWS SDK for PHP
wget https://github.com/aws/aws-sdk-php/releases/download/3.62.3/aws.zip
unzip -q aws.zip -d /var/www/html

# Load Amazon Aurora DB connection details from AWS CloudFormation
un="dbadmin"
pw="lab-password"
ep="inventory-cluster.cluster-cjkkygyjf1aw.us-east-1.rds.amazonaws.com"
db="inventory"
#mysql -u $un -p$pw -h $ep $db < /var/www/html/sql/inventory.sql

# Populate PHP app settings with DB info
sed -i "s/DBENDPOINT/$ep/g" /var/www/html/get-parameters.php
sed -i "s/DBNAME/$db/g" /var/www/html/get-parameters.php
sed -i "s/DBUSERNAME/$un/g" /var/www/html/get-parameters.php
sed -i "s/DBPASSWORD/$pw/g" /var/www/html/get-parameters.php

# Turn on web server
systemctl start httpd.service
systemctl enable httpd.service
```

## Connect to PHP inventory application

[Website URL](Inventory-LB-1531457443.us-east-1.elb.amazonaws.com)
