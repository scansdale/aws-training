# Module 4 - Compute

* What is an AMI?
* What AWS compute service are there?
* How do I decide which EC2 instance types are best for my business?
* How can I optimise cloud compute and its cost, based on my business needs?
* What is a Lambda and how does it work?

## EC2

* [AWS Documentation](https://aws.amazon.com/ec2/resources/?trk=ec2_landing&ar-cards-ec2.sort-by=item.additionalFields.datePublished&ar-cards-ec2.sort-order=desc)
* Secure and resizable compute capacity in the cloud

### AMI (Amazon Machine Image)

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html)
* Template for instance volumes
* Repeatable, reusable, recoverable

### Shared and dedicated tenancy

* Shared tenancy
* Dedicated instance
* Dedicated host

### Types of instance

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html)
* c5n.xlarge
  * `c` Instance family
  * `5` Instance generation
  * `n` Attribute
  * `xlarge` Instance size

### AWS Compute Optimizer

* [AWS Documentation](https://aws.amazon.com/compute-optimizer/)
* Uses machine learning to understand existing usages to recommend best instances

### User data

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html)
* Allows you to create scripts to automate tasks when starting an instance

### Key pair

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html)
* Adds a layer of security by preventing users without the key connecting to the instance

### Tags

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html)
* A way to group and manage lots of services together

### Pricing options

* [AWS Documentation](https://aws.amazon.com/ec2/pricing/?trkCampaign=acq_paid_search_brand&sc_channel=PS&sc_campaign=acquisition_GB&sc_publisher=Google&sc_category=Cloud%20Computing&sc_country=GB&sc_geo=EMEA&sc_outcome=acq&sc_detail=amazon%20aws%20ec2%20pricing&sc_content={ad%20group}&sc_matchtype=e&sc_segment=489216384889&sc_medium=ACQ-P|PS-GO|Brand|Desktop|SU|Cloud%20Computing|EC2|GB|EN|Sitelink&s_kwcid=AL!4422!3!489216384889!e!!g!!amazon%20aws%20ec2%20pricing&ef_id=Cj0KCQiAqbyNBhC2ARIsALDwAsDQDpHAkacnpFkLnlj4xaCbn44MSfjlnzQi-rh1L1fypY11ohg6HegaAnd-EALw_wcB:G:s&s_kwcid=AL!4422!3!489216384889!e!!g!!amazon%20aws%20ec2%20pricing)
* On-demand instances
* Reserved instances
* Savings plans
* Spot instances

## EC2 storage

### AWS EBS

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonEBS.html)
* Cannot be shared between multiple instances
* SSD-backed volumes
  * General Purpose SSD — Provides a balance of price and performance. We recommend these volumes for most workloads
    * gp2 (General Purpose)
    * gp3 (General Purpose)
  * Provisioned IOPS SSD — Provides high performance for mission-critical, low-latency, or high-throughput workloads
    * io1
    * io2
* HDD-backed volumes
  * Throughput Optimized HDD — A low-cost HDD designed for frequently accessed, throughput-intensive workloads
    * st1
  * Cold HDD — The lowest-cost HDD design for less frequently accessed workloads.
    * sc1

### Ephemeral instance store

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html)
* Removed after the instance stops

## HPC (High Performance Computing)

* [AWS Documentation](https://aws.amazon.com/hpc/resources/)
* Elastic Network Interface
* Elastic Network Adapter
* Elastic Fabric Adapter

### Placement groups

* [AWS Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/placement-groups.html)
* Cluster
* Spread
* Partition placement groups

## Lambda function

* [AWS Documentation](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)
* A compute service that lets you run code without provisioning or managing servers
* Can only allocated up to 10GB memory
* Can only run for a maximum of 15 minutes
